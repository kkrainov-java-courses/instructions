# Работа с GitLab Actions и Package Registry (базовая версия)

## Публикация артефакта

**Важно**: это действие выполняется один раз для каждого проекта, который мы планируем переиспользовать (библиотека).

1\. Создайте в корне вашего проекта файл `.gitlab-ci.yml` (файл должен называться именно так с точкой в начале):

```yaml
stages:
  - deploy

deploy-job:
  stage: deploy
  image: maven:3.8-jdk-8-slim
  script:
    - 'echo "Build & deploy application..."'
    - 'mvn -B -s ./settings.xml deploy'
  artifacts:
    paths:
      - ./target/*.jar
  only:
    - main
    - master
```

2\. Создайте в корне вашего проекта файл `settings.xml`:

```xml

<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
    <servers>
        <server>
            <id>gitlab-maven</id>
            <configuration>
                <httpHeaders>
                    <property>
                        <name>Job-Token</name>
                        <value>${env.CI_JOB_TOKEN}</value>
                    </property>
                </httpHeaders>
            </configuration>
        </server>
    </servers>
</settings>
```

3\. Удостоверьтесь, что ваш `pom.xml` выглядит следующим образом:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.example</groupId>
    <artifactId>distance-prediction</artifactId>
    <version>1.0</version>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <!-- FIXME: вы должны заменить на свой Project ID-->
        <gitlab.project.id>32383609</gitlab.project.id>
    </properties>

    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/${gitlab.project.id}/packages/maven</url>
        </repository>
    </repositories>
    <distributionManagement>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/${gitlab.project.id}/packages/maven</url>
        </repository>
        <snapshotRepository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/${gitlab.project.id}/packages/maven</url>
        </snapshotRepository>
    </distributionManagement>

</project>
```

4\. **Важно**: убедитесь, что вы заменили Project ID на свой:

![](project-id.png)

5\. Сделайте `git add .`, `git commit -m "ci: gitlab ci integrated"`, `git push --all`

6\. Удостоверьтесь, что на вкладке `CI/CD` -> `Pipelines` успешно прошла сборка (если же не успешно, то зайдите внутрь задачи и посмотрите, где произошла ошибка):

![](gitlab-actions.png)

7\. Удостоверьтесь, что на вкладке `Packages & Registries` -> `Package Registry` появился ваш артефакт:

![](artefact.png)

## Использование артефакта

1\. Выбираете созданный на предыдущем этапе артефакт и переходите на его страничку:

![](artefact-info.png)

2\. Создаёте новый Maven проект и добавляете туда информацию о репозитории и о зависимости, указанных на предыдущем шаге. Используйте класс `DistanceService` (в качестве примера - [distance-prediction-sample](https://gitlab.com/coursar-java-courses/distance-prediction-example)):

Пример `pom.xml` (вам нужно заменить `gitlab.project.id`):
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.example</groupId>
    <artifactId>distance-prediction-example</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <!-- FIXME: вы должны заменить на свой Project ID-->
        <gitlab.project.id>32383609</gitlab.project.id>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.example</groupId>
            <artifactId>distance-prediction</artifactId>
            <version>1.0</version>
        </dependency>
    </dependencies>

    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/${gitlab.project.id}/packages/maven</url>
        </repository>
    </repositories>
    <distributionManagement>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/${gitlab.project.id}/packages/maven</url>
        </repository>
        <snapshotRepository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/${gitlab.project.id}/packages/maven</url>
        </snapshotRepository>
    </distributionManagement>

</project>
```

Пример `Main.java` (`DistancePredictionService` мы берём из артефакта, опубликованного на GitLab Package Registry - т.е. в этом проекте такого класса быть не должно):
```java
package org.example;

import org.example.service.DistancePredictionService;

public class Main {
  public static void main(String[] args) {
    final DistancePredictionService service = new DistancePredictionService();
    final int predict = service.predict(10, 10);
    System.out.println(predict);
  }
}
```

3\. Если Maven не видит этот класс, убедитесь, что вы указали всю информацию верно, после чего запустите в IDEA: maven reload all projects

4\. Запустите ваше приложение, удостоверьтесь, что оно запускается и работает.
