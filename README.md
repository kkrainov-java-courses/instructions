# Список руководств к курсу Java Developer

1. [Начало работы с GitLab - создание групп и проектов](01_gitlab)
2. [CI/CD на базе GitLab Actions и GitLab Package Registry (базовая версия)](02_cicd) 
3. [JaCoCo & Checkstyle](03_style) 
